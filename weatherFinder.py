#! python3

#watherFinder.py - Shows weather forecast for location from command line
import json, requests, sys

if len(sys.argv) < 2:
    print('Using: weatherFinder.py location')
    sys.exit()
location = ' '.join(sys.argv[1:])

#dowloading data in JSON format from the OpenWeatherMap.org site's API
url = 'http://api.openweathermap.org/data/2.5/weather?q=%s&APPID=527ea61005f5b0c9c0f4dbe918f6fe9c' % (location)
response = requests.get(url)
response.raise_for_status()

#placing JSON data in Python variable
weatherData = json.loads(response.text)

#printing description about current weather
w = weatherData['weather']

print('Current weather for %s:' % (location))
print(w[0]['main'], '-', w[0]['description'])





